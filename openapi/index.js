const info = require('./info');
const servers = require('./servers');
const paths = require('./paths');
const videoSchema = require('./schemas/video');
const statisticSchema = require('./schemas/statistic');


module.exports = {
  openapi: '3.0.1',
  info,
  servers,
  paths,
  components: {
    schemas: {
      ...videoSchema,
      ...statisticSchema,
      Error: {
        type: 'object',
        properties: {
          message: { type: 'string' },
          status: { type: 'string' }
        }
      }
    }
  }
};
