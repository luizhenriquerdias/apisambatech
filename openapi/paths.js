module.exports = {
  '/videos': {
    post: {
      description: 'Insert a video information',
      operationId: 'postVideo',
      requestBody: {
        content: { 'application/json': { schema: { $ref: '#/components/schemas/Video' } } },
        required: true
      },
      responses: {
        200: {
          description: 'Video inserted',
          content: { 'application/json': { schema: { $ref: '#/components/schemas/Video' } } }
        },
        400: {
          description: 'Missing parameters',
          content: 'Missing parameters'
        }
      }
    },
    delete: {
      description: 'Delete all videos',
      operationId: 'deleteAllVideos',
      responses: {
        201: { description: 'All videos informations were deleted' },
        400: 'Error'
      }
    }
  },
  '/statistics': {
    get: {
      description: 'Get statistics',
      operationId: 'getStatistics',
      responses: { 200: { content: { 'application/json': { schema: { $ref: '#/components/schemas/Statistics' } } } } }
    }
  }
};
