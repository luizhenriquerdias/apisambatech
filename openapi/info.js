module.exports = {
  version: '1',
  title: 'Samba Tech',
  description: 'Samba Tech\'s Technical Challenge',
  contact: {
    name: 'Luiz Henrique Dias',
    email: 'luizhenriquerdias@gmail.com',
    url: 'https://linkedin.com/in/luizhenriquerdias/'
  },
  license: {
    name: 'Apache 2.0',
    url: 'https://www.apache.org/licenses/LICENSE-2.0.html'
  }
};
