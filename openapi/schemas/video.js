module.exports = {
  timestamp: {
    type: 'integer',
    description: 'When the video was added (UTC time zone)',
    example: 1478192204000
  },
  duration: {
    type: 'number',
    description: 'Duration of the video',
    example: 200.3
  },
  Video: {
    type: 'object',
    properties: {
      timestamp: { $ref: '#/components/schemas/timestamp' },
      duration: { $ref: '#/components/schemas/duration' }
    }
  },
  Videos: {
    type: 'array',
    items: { $ref: '#/components/schemas/Video' }
  }
};
