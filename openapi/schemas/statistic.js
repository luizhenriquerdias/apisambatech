module.exports = {
  sum: {
    type: 'number',
    description: 'Sum of all video lengths inserted in the last 60 seconds',
    example: 1478192204000
  },
  avg: {
    type: 'number',
    description: 'Average length of uploaded videos in the last 60 seconds.',
    example: 1478192204000
  },
  max: {
    type: 'number',
    description: 'Longer duration of a video inserted in the last 60 seconds',
    example: 1478192204000
  },
  min: {
    type: 'number',
    description: 'Shorter duration of a video inserted in the last 60 seconds',
    example: 1478192204000
  },
  count: {
    type: 'integer',
    description: 'Number of videos inserted in the last 60 seconds',
    example: 1478192204000
  },
  Statistic: {
    type: 'object',
    properties: {
      sum: { $ref: '#/components/schemas/sum' },
      avg: { $ref: '#/components/schemas/avg' },
      max: { $ref: '#/components/schemas/max' },
      min: { $ref: '#/components/schemas/min' },
      count: { $ref: '#/components/schemas/count' }
    }
  },
  Statistics: {
    type: 'array',
    items: { $ref: '#/components/schemas/Statistic' }
  }
};
