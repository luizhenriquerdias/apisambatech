const { calculateStatistics } = require('../../src/functions');

it('should get the statistics based on a valid array of videos', () => {
  const videos = [
    { duration: 100, timestamp: 50 },
    { duration: 90, timestamp: 30 },
    { duration: 110, timestamp: 50 }
  ];
  const statistics = calculateStatistics(videos, 20, 55);
  expect(statistics).toStrictEqual({
    sum: 210,
    avg: 105,
    max: 110,
    min: 100,
    count: 2
  });
});
