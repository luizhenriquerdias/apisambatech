const { storeVideo } = require('../../src/Validators');

it.each([
  { is_valid: false, duration: 100 },
  { is_valid: false, timestamp: 30 },
  { is_valid: false, duration: '110', timestamp: 90000 },
  { is_valid: false, duration: '110', timestamp: '50' },
  { is_valid: false, duration: 110, timestamp: '50' },
  { is_valid: false, duration: 100000, timestamp: 50 },
  { is_valid: true, duration: 1215, timestamp: 99999 },
  { is_valid: true, duration: 5321, timestamp: 88888 },
  { is_valid: false, duration: 100000, timestamp: 121684916 },
  { is_valid: false, duration: 0, timestamp: 90000 },
  { is_valid: false, duration: -5468, timestamp: 90000 }
])(
  'should validate the body of POST /videos',
  body => {
    const is_valid = (!storeVideo(body.timestamp, body.duration, 100000)) === body.is_valid;
    expect(is_valid).toBe(true);
  },
);
