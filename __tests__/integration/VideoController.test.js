const supertest = require('supertest');
const app = require('../../src/server');

const request = supertest(app);

describe('VideoController', () => {
  it('should get the statistics', async done => {
    const response = await request.get('/statistics');
    expect(response.body).toStrictEqual({});
    done();
  });

  it('should delete all videos', async done => {
    const response = await request.delete('/videos');
    expect(response.status).toBe(204);
    done();
  });

  it('should store a video', async done => {
    const response = await request.post('/videos').send({ timestamp: 100, duration: 100 });
    expect(response.status).toBe(204);
    done();
  });
});
