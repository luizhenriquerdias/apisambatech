const swaggerUi = require('swagger-ui-express');
const openApiDocumentation = require('./openapi');
const app = require('./src/server');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(openApiDocumentation));
app.listen(process.env.PORT, process.env.HOST);
