'use strict';

module.exports = {
  calculateStatistics: (videos, threshold, now) => {
    if (!videos.length) return {};
    const statistics = { };
    videos = videos.filter(video => { // Do the calculations while remove old videos
      const is_valid = video.timestamp - now > -threshold; // Check if the video is valid, i.e., was inserted in the last 60 seconds
      if (is_valid) {
        statistics.max = Math.max(statistics.max || null, video.duration);
        statistics.min = Math.min(statistics.min || Infinity, video.duration);
        statistics.sum = (statistics.sum || 0) + video.duration;
      }
      return is_valid;
    });
    statistics.count = videos.length;
    statistics.avg = (statistics.sum / (videos.length || 1));
    return statistics;
  }
};
