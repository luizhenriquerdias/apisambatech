require('dotenv').config();
const cors = require('cors');
const express = require('express');
const VideoController = require('./VideoController');

const app = express();
app.disable('x-powered-by'); // disable x-powered-by because it exposes some informations
app.use(express.json());
app.use(cors());

app.use((req, res, next) => { // Middleware to validate the authorization token
  if (process.env.AUTH_TOKEN)
    if (!req.headers.authorization || req.headers.authorization !== process.env.AUTH_TOKEN)
      return res.status(401).send('');
  return next();
});

app.get('/', (req, res) => res.send('Samba Tech'));
app.get('/statistics', VideoController.statistics);

app.get('/videos', VideoController.index); // Get all videos inserted in the last 60 seconds
app.delete('/videos', VideoController.delete); // Delete all videos
app.post('/videos', VideoController.store); // Push a video

module.exports = app;
