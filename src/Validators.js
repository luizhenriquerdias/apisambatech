'use strict';

const { threshold } = require('./consts');

module.exports = {
  storeVideo: (timestamp, duration, now) => {
    if (typeof duration !== 'number')
      return { status: 400, message: 'Missing duration or wrong format' };

    if (typeof timestamp !== 'number')
      return { status: 400, message: 'Missing timestamp or wrong format' };

    if (duration > 86400) // Considering that duration is in seconds. 24 hours for example
      return { status: 400, message: 'The duration has to be less than 24 hours.' };

    if (duration < 1) // Considering that duration is in seconds
      return { status: 400, message: 'The duration cant be less than 1 second.' };

    if (timestamp > now)
      return { status: 400, message: 'Future timestamp' };

    if (timestamp - now < -threshold) // Dont accept videos with timestamp less than 60 seconds behind now
      return { status: 204, message: '' };

    return null;
  }
};
