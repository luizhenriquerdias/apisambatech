'use strict';

const { storeVideo } = require('./Validators');
const { threshold } = require('./consts');
const { calculateStatistics } = require('./functions');

let videos = [];
let statistics = {};

setInterval(() => { // Set interval to keep the statistics always ready to get
  statistics = calculateStatistics(videos, threshold, new Date().getTime());
}, 200); // The statistics are update every .2 second

module.exports = {
  index: (req, res) => res.json(videos),

  statistics: (req, res) => res.json(statistics),

  store: (req, res) => {
    const { duration, timestamp } = req.body;

    const validation = storeVideo(timestamp, duration, new Date().getTime());
    if (validation) return res.status(validation.status).send(validation.message);

    try {
      videos.push({ duration, timestamp });
    } catch (error) {
      return res.status(400).send();
    }

    return res.status(201).json({ duration, timestamp });
  },

  delete: (req, res) => {
    try {
      videos = [];
    } catch (error) {
      return res.status(400).send();
    }
    return res.status(204).send();
  }
};
