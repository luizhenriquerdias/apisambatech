# Desafio Técnico Samba Tech

This project was made with [ExpressJS](https://expressjs.com/)

## Install
1. Clone the project `https://github.com/luizhenriquerdias/apisambatech.git`
2. `cd apisambatech` and install dependencies `npm install`
3. Run `npm start`

## Run with Docker
- Build the container `docker-compose up -d`

## Tests
- Run the tests `npm test`

## Additional info
- Se the API documentation at /api-docs
- All requests has complexity O(1)
- One of the key principles of Node.js is that all code runs in a single thread, which eliminates the need for the developer to deal with the complexities of writing thread-safe code. So this project is, by definition, thread-safe.
- You can create a simple security layer by adding some string AUTH_TOKEN, in the .env file. If AUTH_TOKEN is defined, the user has to send an Authorization header in every request.
- The CI/CD with deploy was made with [GitLab CI](https://docs.gitlab.com/ee/ci/)